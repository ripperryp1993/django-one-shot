from django.urls import path

from todos.views import TodoItemCreateView, TodoItemUpdateView, TodoListCreateView, TodoListDeleteView, TodoListDetailView, TodoListListView, TodoListUpdateView


urlpatterns = [
    path('', TodoListListView.as_view(), name='todo_lists_list'),
    path('create/', TodoListCreateView.as_view(), name='todo_list_new'),
    path('items/create/', TodoItemCreateView.as_view(), name='todo_item_new'),
    path('<int:pk>/', TodoListDetailView.as_view(), name='todo_list_detail'),
    path('<int:pk>/edit/', TodoListUpdateView.as_view(), name='todo_list_edit'),
    path('<int:pk>/delete/', TodoListDeleteView.as_view(), name='todo_list_delete'),
    path('items/<int:pk>/edit/', TodoItemUpdateView.as_view(), name='todo_item_edit'),
]